#!/usr/bin/env bash
for f in /scripts/*.sh; do
  bash "$f" -H || true
done
exec "$@"
